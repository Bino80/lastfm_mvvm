//
//  LastFMAlbumModelTests.swift
//  LastFMTests
//
//  Created by Binoy Jose on 17/08/2020.
//  Copyright © 2020 Binoy Jose. All rights reserved.
//

import XCTest
@testable import LastFM

class LastFMAlbumModelTests: XCTestCase {
    let oQueryText = "Query"
    let oQueryRole = "request"
    let oQuerySearchTerms = "believe"
    let oQueryStartPage = "1"
    
    let opensearchTotalResults = "734"
    let opensearchStartIndex = "0"
    let opensearchItemsPerPage = "20"
    
    let sizeRawValueExtraLarge = "extralarge"
    let sizeRawValueLarge = "large"
    let sizeRawValueMedium = "medium"
    let sizeRawValueSmall = "small"
    let imageText = "http://userserve-ak.last.fm/serve/64/8673675.jpg"
    
    let albumName = "Make Believe"
    let albumArtist = "Weezer"
    let albumURL = "http://www.last.fm/music/Weezer/Make+Believe"
    let albumStreamable = "0"
    let albumMbid = "2025180"
    let attrFor = "attrFor"
    
    var results:Results?
    var opensearchQuery:OpensearchQuery?
    var albummatches:Albummatches?
    var album:Album?
    var image:Image?
    var size:Size?
    var attr:Attr?
    
    override func setUpWithError() throws {
        opensearchQuery = OpensearchQuery(text: oQueryText, role: oQueryRole, searchTerms: oQuerySearchTerms, startPage: oQueryStartPage)
        size = Size(rawValue: sizeRawValueLarge)
        image = Image(text: imageText, size: size!)
        album = Album(name: albumName, artist: albumArtist, url: albumArtist, image: [image!], streamable: albumStreamable, mbid: albumMbid)
    }
    
    override func tearDownWithError() throws {
        results = nil
        opensearchQuery = nil
        size = nil
        image = nil
        album = nil
        albummatches = nil
        attr = nil
    }
    
    func testOpensearchQueryModel() {
        opensearchQuery = OpensearchQuery(text: oQueryText, role: oQueryRole, searchTerms: oQuerySearchTerms, startPage: oQueryStartPage)
        
        XCTAssertEqual(opensearchQuery?.text,oQueryText, "expected OpensearchQuery text is \(oQueryText)")
        XCTAssertEqual(opensearchQuery?.role,oQueryRole, "expected OpensearchQuery role is \(oQueryRole)")
        XCTAssertEqual(opensearchQuery?.searchTerms,oQuerySearchTerms, "expected OpensearchQuery searchTerms is \(oQuerySearchTerms)")
        XCTAssertEqual(opensearchQuery?.startPage,oQueryStartPage, "expected OpensearchQuery startPage is \(oQueryStartPage)")
    }
    
    func testAttrModel() {
        attr = Attr(attrFor: attrFor)
        
        XCTAssertEqual(attr!.attrFor,attrFor, "expected Attr attrFor is \(attrFor)")
    }
    
    func testSizeModel() {
        let sizeExtraLarge = Size(rawValue: sizeRawValueExtraLarge)
        XCTAssertEqual(sizeExtraLarge?.rawValue,sizeRawValueExtraLarge, "expected Size rawValue is \(sizeRawValueExtraLarge)")
        
        let sizeLarge = Size(rawValue: sizeRawValueLarge)
        XCTAssertEqual(sizeLarge?.rawValue,sizeRawValueLarge, "expected Size rawValue is \(sizeRawValueLarge)")
        
        let sizeMedium = Size(rawValue: sizeRawValueMedium)
        XCTAssertEqual(sizeMedium?.rawValue,sizeRawValueMedium, "expected Size rawValue is \(sizeRawValueMedium)")
        
        let sizeSmall = Size(rawValue: sizeRawValueSmall)
        XCTAssertEqual(sizeSmall?.rawValue,sizeRawValueSmall, "expected Size rawValue is \(sizeRawValueSmall)")
    }
    
    func testImageModel(){
        size = Size(rawValue: sizeRawValueLarge)
        image = Image(text: imageText, size: size!)
        
        XCTAssertEqual(image?.size.rawValue,sizeRawValueLarge, "expected Image Size rawValue is \(sizeRawValueLarge)")
        XCTAssertEqual(image?.text,imageText, "expected Image text is \(imageText)")
    }
    
    func testAlbumModel(){
        size = Size(rawValue: sizeRawValueLarge)
        image = Image(text: imageText, size: size!)
        album = Album(name: albumName, artist: albumArtist, url: albumURL, image: [image!], streamable: albumStreamable, mbid: albumMbid)
        
        XCTAssertEqual(image?.size.rawValue,sizeRawValueLarge, "expected Image Size rawValue is \(sizeRawValueLarge)")
        XCTAssertEqual(image?.text,imageText, "expected Image text is \(imageText)")
        
        XCTAssertEqual(album?.name,albumName, "expected Album name is \(albumName)")
        XCTAssertEqual(album?.artist,albumArtist, "expected Album artist is \(albumArtist)")
        XCTAssertEqual(album?.url,albumURL, "expected Album name is \(albumURL)")
        XCTAssertEqual(album?.streamable,albumStreamable, "expected Album streamable is \(albumStreamable)")
        XCTAssertEqual(album?.mbid,albumMbid, "expected Album mbid is \(albumName)")
    }
    
    func testAlbummatchesModel(){
        size = Size(rawValue: sizeRawValueLarge)
        image = Image(text: imageText, size: size!)
        album = Album(name: albumName, artist: albumArtist, url: albumURL, image: [image!], streamable: albumStreamable, mbid: albumMbid)
        let albumArray = [album!]
        let albumArrayCount = albumArray.count
        albummatches = Albummatches(album: albumArray)
        
        XCTAssertEqual(image?.size.rawValue,sizeRawValueLarge, "expected Image Size rawValue is \(sizeRawValueLarge)")
        XCTAssertEqual(image?.text,imageText, "expected Image text is \(imageText)")
        
        XCTAssertEqual(album?.name,albumName, "expected Album name is \(albumName)")
        XCTAssertEqual(album?.artist,albumArtist, "expected Album artist is \(albumArtist)")
        XCTAssertEqual(album?.url,albumURL, "expected Album name is \(albumURL)")
        XCTAssertEqual(album?.streamable,albumStreamable, "expected Album streamable is \(albumStreamable)")
        XCTAssertEqual(album?.mbid,albumMbid, "expected Album mbid is \(albumName)")
        
        XCTAssertEqual(albummatches?.album.count,albumArrayCount, "expected albummatches  is \(albumArrayCount)")
    }
    
    func testResultsModel(){
        size = Size(rawValue: sizeRawValueLarge)
        image = Image(text: imageText, size: size!)
        album = Album(name: albumName, artist: albumArtist, url: albumURL, image: [image!], streamable: albumStreamable, mbid: albumMbid)
        let albumArray:Array = [album!]
        let albumArrayCount = albumArray.count
        albummatches = Albummatches(album: albumArray)
        attr = Attr(attrFor: attrFor)
        
        opensearchQuery = OpensearchQuery(text: oQueryText, role: oQueryRole, searchTerms: oQuerySearchTerms, startPage: oQueryStartPage)
        results = Results(opensearchQuery: opensearchQuery!, opensearchTotalResults: opensearchTotalResults, opensearchStartIndex: opensearchStartIndex, opensearchItemsPerPage: opensearchItemsPerPage, albummatches: albummatches!, attr: attr!)
        
        XCTAssertEqual(image?.size.rawValue,sizeRawValueLarge, "expected Image Size rawValue is \(sizeRawValueLarge)")
        XCTAssertEqual(image?.text,imageText, "expected Image text is \(imageText)")
        
        XCTAssertEqual(album?.name,albumName, "expected Album name is \(albumName)")
        XCTAssertEqual(album?.artist,albumArtist, "expected Album artist is \(albumArtist)")
        XCTAssertEqual(album?.url,albumURL, "expected Album name is \(albumURL)")
        XCTAssertEqual(album?.streamable,albumStreamable, "expected Album streamable is \(albumStreamable)")
        XCTAssertEqual(album?.mbid,albumMbid, "expected Album mbid is \(albumName)")
        
        XCTAssertEqual(results?.albummatches.album.count,albumArrayCount, "expected albummatches  is \(albumArrayCount)")
        
        XCTAssertEqual(results?.attr.attrFor,attrFor, "expected Attr attrFor is \(attrFor)")
        XCTAssertEqual(results?.opensearchQuery.text,oQueryText, "expected OpensearchQuery text is \(oQueryText)")
        XCTAssertEqual(results?.opensearchQuery.role,oQueryRole, "expected OpensearchQuery role is \(oQueryRole)")
        XCTAssertEqual(results?.opensearchQuery.searchTerms,oQuerySearchTerms, "expected OpensearchQuery searchTerms is \(oQuerySearchTerms)")
        XCTAssertEqual(results?.opensearchQuery.startPage,oQueryStartPage, "expected OpensearchQuery startPage is \(oQueryStartPage)")
        
        
        XCTAssertEqual(results?.opensearchTotalResults,opensearchTotalResults, "expected opensearchTotalResults is \(opensearchTotalResults)")
        XCTAssertEqual(results?.opensearchStartIndex,opensearchStartIndex, "expected OopensearchStartIndex is \(opensearchStartIndex)")
        XCTAssertEqual(results?.opensearchItemsPerPage,opensearchItemsPerPage, "expected opensearchItemsPerPage is \(opensearchItemsPerPage)")
    }
}

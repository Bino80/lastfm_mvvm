//
//  AlbumViewControllerModelTests.swift
//  LastFMTests
//
//  Created by Binoy Jose on 17/08/2020.
//  Copyright © 2020 Binoy Jose. All rights reserved.
//

import XCTest
@testable import LastFM

class AlbumViewControllerModelTests: XCTestCase {
    let albumViewControllerModel = AlbumViewControllerModel()
    
    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testIsNotEmptyWithEmptyString() {
        let isEmpty = albumViewControllerModel.isNotEmpty(string: "")
        let expectedResult = false
        XCTAssertEqual(isEmpty, expectedResult, "Expected result is \(expectedResult)" )
    }
    
    func testIsNotEmptyWithString() {
        let isEmpty = albumViewControllerModel.isNotEmpty(string: "TestData")
        let expectedResult = true
        XCTAssertEqual(isEmpty, expectedResult, "Expected result is \(expectedResult)" )
    }

    func testIsNullWithNullString() {
        let isNull = albumViewControllerModel.isNull(string: "(null)")
        let expectedResult = true
        XCTAssertEqual(isNull, expectedResult, "Expected result is \(expectedResult)" )
    }
    
    func testIsNullWithOutNullString() {
        let isNull = albumViewControllerModel.isNull(string: "TestData")
        let expectedResult = false
        XCTAssertEqual(isNull, expectedResult, "Expected result is \(expectedResult)" )
    }
    
    func testIsNullWithEmptyString() {
        let isNull = albumViewControllerModel.isNull(string: "")
        let expectedResult = false
        XCTAssertEqual(isNull, expectedResult, "Expected result is \(expectedResult)" )
    }
    
    func testVerifiedLabelTextWithEmptyString() {
        let verifiedLabelText = albumViewControllerModel.verifiedLabelText(value: "", defaultValue: "not known")
        let expectedResult = "not known"
        XCTAssertEqual(verifiedLabelText, expectedResult, "Expected result is \(expectedResult)" )
    }
    
    func testVerifiedLabelTextWithString() {
        let verifiedLabelText = albumViewControllerModel.verifiedLabelText(value: "TestData", defaultValue: "not known")
        let expectedResult = "TestData"
        XCTAssertEqual(verifiedLabelText, expectedResult, "Expected result is \(expectedResult)" )
    }
    
    func testVerifiedLabelTextWithNullString() {
        let verifiedLabelText = albumViewControllerModel.verifiedLabelText(value: "(null)", defaultValue: "not known")
        let expectedResult = "not known"
        XCTAssertEqual(verifiedLabelText, expectedResult, "Expected result is \(expectedResult)" )
    }
}

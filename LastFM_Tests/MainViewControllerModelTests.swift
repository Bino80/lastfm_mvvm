//
//  MainViewControllerModelTests.swift
//  LastFMTests
//
//  Created by Binoy Jose on 16/08/2020.
//  Copyright © 2020 Binoy Jose. All rights reserved.
//

import XCTest
@testable import LastFM

class MainViewControllerModelTests: XCTestCase {
    let mainViewControllerModel = MainViewControllerModel()
    var opensearchQuery:OpensearchQuery?
    var albummatches:Albummatches?
    var album:Album?
    var image:Image?
    var size:Size?
    
    override func setUpWithError() throws {
        opensearchQuery = OpensearchQuery(text: "Query", role: "request", searchTerms: "believe", startPage: "1")
        size = Size(rawValue: "large")
        image = Image(text: "http://userserve-ak.last.fm/serve/64/8673675.jpg", size: size!)
        album = Album(name: "Make Believe", artist: "Weezer", url: "http://www.last.fm/music/Weezer/Make+Believe", image: [image!], streamable: "0", mbid: "2025180")
    }
    
    override func tearDownWithError() throws {
        opensearchQuery = nil
        size = nil
        image = nil
        album = nil
        albummatches = nil
    }
    
    func testTableCellName() throws {
        let tableCellName = mainViewControllerModel.tableCellName
        let expectedTableCellName = "AlbumTableCell"
        XCTAssertEqual(tableCellName, expectedTableCellName, "Expected tableCellName is \(expectedTableCellName)" )
    }
    
    func testCellReuseId() {
        let cellReuseId = mainViewControllerModel.cellReuseId
        let expectedCellReuseId = "reuseIdentifier"
        XCTAssertEqual(cellReuseId, expectedCellReuseId, "Expected tableCellName is \(expectedCellReuseId)" )
    }
    
    func testAlbumCountTestWithoutAlbum() {
        albummatches = Albummatches(album: [])
        let fMAlbum = LastFMAlbum(results: Results(opensearchQuery:opensearchQuery! , opensearchTotalResults: "745", opensearchStartIndex: "0", opensearchItemsPerPage: "20", albummatches:albummatches! , attr: Attr(attrFor: "attrFor")))
        
        let albumCount = mainViewControllerModel.albumCount(data: fMAlbum)
        let expectedAlbumCount = 0
        
        XCTAssertEqual(albumCount, expectedAlbumCount, "Expected albumCount is \(expectedAlbumCount)" )
    }
    
    func testAlbumCountTestWithOneAlbum() {
        albummatches = Albummatches(album: [album!])
        let fMAlbum = LastFMAlbum(results: Results(opensearchQuery:opensearchQuery! , opensearchTotalResults: "745", opensearchStartIndex: "0", opensearchItemsPerPage: "20", albummatches:albummatches! , attr: Attr(attrFor: "attrFor")))
        
        let albumCount = mainViewControllerModel.albumCount(data: fMAlbum)
        let expectedAlbumCount = 1
        
        XCTAssertEqual(albumCount, expectedAlbumCount, "Expected albumCount is \(expectedAlbumCount)" )
    }
    
    func testAlbumCountTestWithTwoAlbum() {
        albummatches = Albummatches(album: [album!,album!])
        let fMAlbum = LastFMAlbum(results: Results(opensearchQuery:opensearchQuery! , opensearchTotalResults: "745", opensearchStartIndex: "0", opensearchItemsPerPage: "20", albummatches:albummatches! , attr: Attr(attrFor: "attrFor")))
        
        let albumCount = mainViewControllerModel.albumCount(data: fMAlbum)
        let expectedAlbumCount = 2
        
        XCTAssertEqual(albumCount, expectedAlbumCount, "Expected albumCount is \(expectedAlbumCount)" )
    }
    
}

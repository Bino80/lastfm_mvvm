//
//  UIViewController+Helper.swift
//  LastFM
//
//  Created by Binoy Jose on 16/08/2020.
//  Copyright © 2020 Binoy Jose. All rights reserved.
//

import UIKit

extension UIViewController {
   public func showAlert(title: String?, message: String?) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default))
        self.present(alert, animated: true)
    }
}

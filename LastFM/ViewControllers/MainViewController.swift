//
//  ViewController.swift
//  LastFM
//
//  Created by Binoy Jose on 16/08/2020.
//  Copyright © 2020 Binoy Jose. All rights reserved.
//

import UIKit

class MainViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate  {
    let mainViewControllerModel = MainViewControllerModel()
    var albumViewControllerModel = AlbumViewControllerModel()
    
    @IBOutlet weak var albumTableView: UITableView!
    @IBOutlet weak var searchTextField: UITextField!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    @IBAction func searchButton(_ sender: Any) {
        startSearch()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        UITextField.appearance().tintColor = .lightGray
        self.registerTableViewCells()
        stopActivityIndicator()
    }
    
    func startSearch() {
        if setSearchText(searchString: removeSpacesFromFrontAndBack(stringValue: searchTextField.text!)) {
            JSONHandler.fetchDataFromJSONinBackground { (error) in
                if error == nil {
                    //Reload your table here
                    DispatchQueue.main.async{
                        self.albumTableView.reloadData()
                        self.stopActivityIndicator()
                    }
                }
            }
        }
    }
    
    func showDetailVC(albumID:Int) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "AlbumDetailViewController") as! AlbumDetailViewController
        vc.albumDetails = (dataFromJSON?.results.albummatches.album[albumID])! as Album
        self.present(vc, animated: true, completion: nil)
    }
    
    func setSearchText(searchString:String) -> Bool {
        if searchString.count > 0 {
            startActivityIndicator()
            searchText = searchString
            return true
        } else {
            stopActivityIndicator()
            self.showAlert(title: "Search string is empty", message: "please enter artist, album or song name")
            return false
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {   //delegate method
        startSearch()
        textField.resignFirstResponder()
        return true
    }
    
    func startActivityIndicator() {
        self.activityIndicator.startAnimating()
    }
    
    func stopActivityIndicator() {
        self.activityIndicator.stopAnimating()
    }
    
    private func registerTableViewCells() {
        let productCell = UINib(nibName: mainViewControllerModel.tableCellName, bundle: nil)
        self.albumTableView.register(productCell, forCellReuseIdentifier: mainViewControllerModel.cellReuseId)
    }
    
    @objc func buttonClicked(sender:UIButton) {
        showDetailVC(albumID: sender.tag)
    }
    
    // This function helps to remove the whitespaces from the beginning and end o
    func removeSpacesFromFrontAndBack(stringValue:String) -> String {
        return stringValue.trimmingCharacters(in: CharacterSet.whitespaces)
    }
    
    // MARK: - Table view delegate methods
    func numberOfSections(in tableView: UITableView) -> Int {
        var numOfSections: Int = 0
        
        if mainViewControllerModel.albumCount(data: dataFromJSON) > 0
        {
            numOfSections            = 1
            tableView.backgroundView = nil
        } else {
            tableView.backgroundView  = makeNoDataLabel(width: tableView.bounds.size.width, height: tableView.bounds.size.height)
        }
        return numOfSections
    }
    
    func makeNoDataLabel(width:CGFloat, height:CGFloat) -> UILabel {
        let noDataLabel: UILabel = UILabel(frame: CGRect(x: 0, y: 0, width:width , height:height ))
        noDataLabel.text        = "No data available"
        noDataLabel.textColor    = UIColor.black
        noDataLabel.textAlignment = .center
        return noDataLabel
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return mainViewControllerModel.albumCount(data: dataFromJSON)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: mainViewControllerModel.cellReuseId) as? AlbumTableCell {
            if let album = dataFromJSON?.results.albummatches.album[indexPath.row] {
                cell.startActivityIndicator()
                cell.nameLabel.text = albumViewControllerModel.verifiedLabelText(value: album.name, defaultValue: defaultLabelText)
                cell.artistLabel.text = albumViewControllerModel.verifiedLabelText(value: album.artist, defaultValue: defaultLabelText)
                
                self.activityIndicator.style = .large
                let urlString = album.image[2].text
                if urlString.count > 0 {
                    let  imageUrl = URL(string:urlString)
                    cell.albumImageView.load(url: imageUrl!)
                } else {
                    cell.albumImageView.image = UIImage(named: "noPreview")
                }
                cell.stopActivityIndicator()
                cell.moreInfoButton.tag = indexPath.row
                cell.moreInfoButton.addTarget(self, action: #selector(buttonClicked), for: UIControl.Event.touchUpInside)
                return cell
            }
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        showDetailVC(albumID: indexPath.row)
    }
}


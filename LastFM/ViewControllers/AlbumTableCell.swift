//
//  AlbumTableCell.swift
//  LastFM
//
//  Created by Binoy Jose on 16/08/2020.
//  Copyright © 2020 Binoy Jose. All rights reserved.
//

import Foundation
import UIKit

class AlbumTableCell: UITableViewCell {
    
    var actionBlock: (() -> Void)? = nil
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var artistLabel: UILabel!
    @IBOutlet weak var moreInfoButton: UIButton!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var albumImageView: UIImageView!
    @IBOutlet var cellView: UIView! {
        didSet {
            cellView.backgroundColor = .clear
            cellView.layer.cornerRadius = 7
            cellView.layer.borderWidth = 1
            cellView.layer.borderColor = UIColor.lightGray.cgColor
        }
    }
    
    func startActivityIndicator() {
        self.activityIndicator.startAnimating()
    }
    
    func stopActivityIndicator() {
        self.activityIndicator.stopAnimating()
    }
}

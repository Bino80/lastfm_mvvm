//
//  AlbumDetailViewController.swift
//  LastFM
//
//  Created by Binoy Jose on 16/08/2020.
//  Copyright © 2020 Binoy Jose. All rights reserved.
//

import UIKit

class AlbumDetailViewController: UIViewController {
    var albumDetails:Album?
    var albumViewControllerModel = AlbumViewControllerModel()
    
    @IBOutlet weak var albumImageView: UIImageView!
    @IBOutlet weak var albumNameLabel: UILabel!
    @IBOutlet weak var artistNameLabel: UILabel!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var idLabel: UILabel!
    @IBOutlet weak var urlLabel: UILabel!
    
    @IBAction func closeButton(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.activityIndicator.style = .large
        startActivityIndicator()
        setupAlbumDetails()
    }
    
    func setupAlbumDetails()  {
        if let urlString = albumDetails?.image[2].text, urlString.count > 0 {
            let  imageUrl = URL(string:urlString)
            albumImageView.load(url: imageUrl!)
        } else {
            albumImageView.image = UIImage(named: "noPreview")
        }
        stopActivityIndicator()
        idLabel.text = albumViewControllerModel.verifiedLabelText(value: albumDetails?.mbid, defaultValue: defaultLabelText)
        urlLabel.text = albumViewControllerModel.verifiedLabelText(value: albumDetails?.url, defaultValue: defaultLabelText)
        albumNameLabel.text = albumViewControllerModel.verifiedLabelText(value: albumDetails?.name, defaultValue: defaultLabelText)
        artistNameLabel.text = albumViewControllerModel.verifiedLabelText(value: albumDetails?.artist, defaultValue: defaultLabelText)
    }
    
    func startActivityIndicator() {
        self.activityIndicator.startAnimating()
    }
    
    func stopActivityIndicator() {
        self.activityIndicator.stopAnimating()
    }
}

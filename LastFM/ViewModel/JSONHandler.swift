//
//  JSONHandler.swift
//  LastFM
//
//  Created by Binoy Jose on 16/08/2020.
//  Copyright © 2020 Binoy Jose. All rights reserved.
//

import Foundation

class JSONHandler {
    
    // Fetch data from given URL in background
    class func fetchConnectionsJSONinBackground(completion: @escaping (LastFMAlbum?, Error?) -> ()) {
        let albumQuery = "?method=album.search&album=\(searchText)&api_key=\(lastFmApiKey)&format=json"
        let urlString = baseURLString + albumQuery
        guard let url = URL(string: urlString) else {
            return
        }
        
        DispatchQueue.global(qos: .background).async {
            URLSession.shared.dataTask(with: url) { (data, resp, error) in
                
                if let error = error {
                    completion(nil,error)
                    return
                }
                
                // successful
                do {
                    let connections = try JSONDecoder().decode(LastFMAlbum.self, from: data!)
                    completion(connections, nil)
                    
                } catch let jsonError {
                    completion(nil,jsonError)
                }
            }.resume()
        }
    }
    
    // Adding data from JSON to to suggested Countries Dictionary
   class func fetchDataFromJSONinBackground(completion: @escaping (Error?) -> ()) {
            JSONHandler.fetchConnectionsJSONinBackground{ (response, error) in
                if let error = error {
                    print("Failed to fetch album", error)
                    completion(error)
                    return
                } else {
                    dataFromJSON = response!
                    completion(nil)
                }
            }
        }
}

//
//  AlbumViewControllerModel.swift
//  LastFM
//
//  Created by Binoy Jose on 16/08/2020.
//  Copyright © 2020 Binoy Jose. All rights reserved.
//

import Foundation

struct AlbumViewControllerModel {
    func verifiedLabelText(value:String?, defaultValue:String) -> String{
        if  isNotEmpty(string: value) && !isNull(string: value){
            return value!
        } else {
            return defaultValue
        }
    }

    func isNotEmpty(string:String?) -> Bool {
        return string?.count ?? 0 > 0
    }

    func isNull(string:String?) -> Bool {
        return string?.contains("null")  ?? true
    }
}

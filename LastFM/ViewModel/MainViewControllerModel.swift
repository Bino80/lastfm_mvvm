//
//  MainViewControllerModel.swift
//  LastFM
//
//  Created by Binoy Jose on 16/08/2020.
//  Copyright © 2020 Binoy Jose. All rights reserved.
//

import Foundation

struct MainViewControllerModel {
    let tableCellName = "AlbumTableCell"
    let cellReuseId = "reuseIdentifier"
    
    func albumCount(data:LastFMAlbum?) -> Int {
        return data?.results.albummatches.album.count ?? 0
    }
}

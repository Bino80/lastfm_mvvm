# README #

## This is a last.fm search mobile App (https://www.last.fm/home) ##

### I have developed as an exercise to refresh skills in public API’s (http://www.last.fm/api/show/album.search) and MVVM design pattern. ###

With this App, the  user will be able to search with an artist, album or songs  as a keyword.

The user will be able to see information about the artist, songs and album and artwork (if any artwork work is available) with the search result.

Once the user selects from the search result it will then display additional basic information about the artist, album and songs.


### How do I get set up? ###

* This project is build in Xcode_11.4.1 and Swift 5+